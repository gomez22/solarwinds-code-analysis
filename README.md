# SolarWinds Code Analysis

The SolarWinds/FireEye attack was a high-profile cybersecurity incident orchestrated by the Russian Government. This project seeks to identify the similarities and differences between the malware and legitimate code.